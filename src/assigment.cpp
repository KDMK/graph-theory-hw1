#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<list>
#include<iterator>
#include<iostream>
#include<cstring>

using namespace std;

void printMatrix(int *adjacencyMatrix, int dim) {
    for(int i = 0; i < dim; i++) {
        for(int j = 0; j < dim; j++) {
            printf("%2d ", *(adjacencyMatrix + i*dim + j));
        }
        printf("\n");
    }
}

//#############################################################################

void getCofactor(int *matrix, int *temp, int cofX, int cofY, int n) 
{ 
    int i = 0, j = 0; 
  
    // Looping for each element of the matrix 
    for (int row = 0; row < n; row++) 
    { 
        for (int col = 0; col < n; col++) 
        { 
            //  Copying into temporary matrix only those element 
            //  which are not in given row and column 
            if (row != cofX && col != cofY) 
            { 
                *(temp + i*(n-1) + j++) = *(matrix + row*n + col); 
  
                // Row is filled, so increase row index and 
                // reset col index 
                if (j == n - 1) 
                { 
                    j = 0; 
                    i++; 
                } 
            } 
        } 
    } 
} 

int calculateDeterminant(int *matrix, int dim) {
    int determinant = 0;

    //  Base case : if matrix contains single element 
    if (dim == 1) 
        return *matrix; 
  
    int *temp = (int*) malloc(dim*dim*sizeof(int)); // To store cofactors 
    int sign = 1;  // To store sign multiplier 
  
     // Iterate for each element of first row 
    for (int f = 0; f < dim; f++) 
    { 
        // Getting Cofactor of mat[0][f] 
        getCofactor(matrix, temp, 0, f, dim); 
        determinant += sign * *(matrix + 0*dim + f) 
                         * calculateDeterminant(temp, dim - 1); 
  
        // terms are to be added with alternate sign 
        sign = -sign; 
    } 

    delete(temp);
  
    return determinant;
}

int numOfSpanningTrees(int *adjacencyMatrix, int dim) {
    int i, j, rowSum, spanningTrees = 0;
    int* Q = (int*) malloc(dim*dim*sizeof(int));
    int* Qp = (int*) malloc((dim-1)*(dim-1)*sizeof(int));

    // 1. Replace all the diagonal elements with the degree of nodes
    // 2. Replace all non-diagonal 1's with -1
    for(i = 0; i < dim; i++) {
        rowSum = 0;
        for(j = 0; j < dim; j++) {
            printf("%d\n", *(adjacencyMatrix + i*dim + j));
            rowSum += *(adjacencyMatrix + i*dim + j);
            if(*(adjacencyMatrix + i*dim + j) == 1) *(Q + i*dim + j) = -1;
            else *(Q + i*dim + j) = 0;
        }
        *(Q + i*dim + i) = rowSum;
    }

    // 3. Calculate co-factor for any element
    // 3.1 Construct Qp by removing last row, and last column
    int newDim = dim -1;
    for(i = 0; i < newDim; i++) {
        for(j = 0;j < newDim; j++) {
            *(Qp + i*newDim + j) = *(Q + i*dim + j);
        }
    }

    // 4. Return the cofactor that you get, this is total number of spanning 
    //    trees
    spanningTrees = calculateDeterminant(Qp, newDim);
    
    delete(Q);
    delete(Qp);

    return spanningTrees;
}

// ############################################################################
// Find minimal spanning tree; part A, union-find algorithm
typedef struct Edge { 
    int src, dest;
    int weight;
} Edge;

typedef struct Graph {
    int numOfVertices;
    int numOfEdges;
    Edge* edges;
} Graph;

typedef struct subset {
    int parent;
    int rank;
} Subset;

Graph* createGraph(int numOfVertices, int numOfEdges) {
    Graph* graph = (Graph*) malloc(sizeof(Graph));
    graph->numOfVertices = numOfVertices;
    graph->numOfEdges = numOfEdges;

    graph->edges = (Edge*) malloc(graph->numOfEdges * sizeof(Edge));

    return graph;
}

int find(int parent[], int i) { 
    if (parent[i] == -1) 
        return i; 
    return find(parent, parent[i]); 
}

int find(Subset subsets[], int i) { 
    // find root and make root as parent of i (path compression) 
    if (subsets[i].parent != i) {
        subsets[i].parent = find(subsets, subsets[i].parent); 
    }
    return subsets[i].parent; 
} 

void Union(Subset subsets[], int x, int y) { 
    int xroot = find(subsets, x); 
    int yroot = find(subsets, y); 
  
    // Attach smaller rank tree under root of high rank tree 
    // (Union by Rank) 
    if (subsets[xroot].rank < subsets[yroot].rank) { 
        subsets[xroot].parent = yroot; 
    }
    else if (subsets[xroot].rank > subsets[yroot].rank) { 
        subsets[yroot].parent = xroot; 
    }
  
    // If ranks are same, then make one as root and increment 
    // its rank by one 
    else { 
        subsets[yroot].parent = xroot; 
        subsets[xroot].rank++; 
    }
} 

// The main function to check whether a given graph contains  
// cycle or not 
int isCycle( struct Graph* graph ) 
{ 
    int vertices = graph->numOfVertices; 
    int edges = graph->numOfEdges; 
  
    // Allocate memory for creating V sets 
    Subset *subsets = (Subset*) malloc( vertices * sizeof(Subset) ); 
  
    for (int v = 0; v < vertices; ++v) 
    { 
        subsets[v].parent = v; 
        subsets[v].rank = 0; 
    } 
  
    // Iterate through all edges of graph, find sets of both 
    // vertices of every edge, if sets are same, then there is 
    // cycle in graph. 
    for(int e = 0; e < edges; ++e) 
    { 
        int x = find(subsets, graph->edges[e].src); 
        int y = find(subsets, graph->edges[e].dest); 
  
        if (x == y) 
            return 1; 
  
        Union(subsets, x, y); 
    } 
    return 0; 
} 


// Part B, Kruskal algorithm
// Compare two edges according to their weights. 
// Used in qsort() for sorting an array of edges 
int myComp(const void* a, const void* b) 
{ 
    struct Edge* a1 = (struct Edge*)a; 
    struct Edge* b1 = (struct Edge*)b; 
    return a1->weight > b1->weight; 
} 

// The main function to construct MST using Kruskal's algorithm 
void KruskalMST(struct Graph* graph) 
{ 
    int vertices = graph->numOfVertices; 
    struct Edge result[vertices];  // Tnis will store the resultant MST 
    int e = 0;  // An index variable, used for result[] 
    int i = 0;  // An index variable, used for sorted edges 
  
    // Step 1:  Sort all the edges in non-decreasing  
    // order of their weight. If we are not allowed to  
    // change the given graph, we can create a copy of 
    // array of edges 
    qsort(graph->edges, graph->numOfEdges, sizeof(graph->edges[0]), myComp); 
  
    // Allocate memory for creating V ssubsets 
    Subset *subsets = (Subset*) malloc( vertices * sizeof(Subset) ); 
  
    // Create V subsets with single elements 
    for (int v = 0; v < vertices; ++v) 
    { 
        subsets[v].parent = v; 
        subsets[v].rank = 0; 
    } 
  
    // Number of edges to be taken is equal to V-1 
    while (e < vertices - 1) 
    { 
        // Step 2: Pick the smallest edge. And increment  
        // the index for next iteration 
        Edge next_edge = graph->edges[i++]; 
  
        int x = find(subsets, next_edge.src); 
        int y = find(subsets, next_edge.dest); 
  
        // If including this edge does't cause cycle, 
        // include it in result and increment the index  
        // of result for next edge 
        if (x != y) 
        { 
            result[e++] = next_edge; 
            Union(subsets, x, y); 
        } 
        // Else discard the next_edge 
    } 
  
    // print the contents of result[] to display the 
    // built MST 
    printf("Following are the edges in the constructed MST\n"); 
    for (i = 0; i < e; ++i) 
        printf("%d -- %d == %d\n", result[i].src, result[i].dest, 
                                                 result[i].weight); 
    return; 
} 

// ############################################################################

void showlist(list <Edge> g) 
{ 
    list <Edge> :: iterator it; 
    for(it = g.begin(); it != g.end(); ++it) 
        cout << '(' << it->src << ", " << it->dest << ')' << endl; 
    cout << '\n'; 
} 

int main(int argc, char **argv) {
    if(argc != 2) {
        printf("Usage: kruskal /path/to/your/input matrix\n");
        printf("Input file should be formatted as follows: \n");
        printf("4\n");
        printf("\n");
        printf("0,0,1,1\n");
        printf("0,0,1,1\n");
        printf("1,1,0,1\n");
        printf("1,1,1,0\n");
        printf("First line represents number of vertices followed by blank line\n");
        printf("and then adjacency matrix as comma-separated values splitted by row\n");
        exit(0);
    }

    ifstream inputFile;
    inputFile.open(argv[1]);

    string line;
    int dim;
    int *adjacencyMatrix;
    if(inputFile.is_open()) {
        getline(inputFile, line);
        dim = stoi(line);
        adjacencyMatrix = (int*) malloc(dim*dim*sizeof(int));

        getline(inputFile,line);
        if(line.compare("") != 0) {
            cout << "Bad input format" << endl;
        }

        for(int i = 0; i < dim; i++) {
            getline(inputFile, line);
            for(int j = 0; j < line.length(); j+=2) {
                *(adjacencyMatrix + i*dim + j/2) = line.at(j) - 48;
            }
        }
        inputFile.close();
    } else {
        cout << "Unable to open file" << endl;
        exit(1);
    }

    printf("Number of spanning trees : %d\n", 
            numOfSpanningTrees(adjacencyMatrix, dim)); 

    int numOfEdges = 0;
    list<Edge> edges;
    for(int i = 0; i < dim; i++) {
        for(int j = 0; j < i; j++) {
            if(*(adjacencyMatrix + i*dim + j) == 1) {
                Edge e;
                e.src = i;
                e.dest = j;
                edges.push_back(e);
                numOfEdges++;
            }
        }
    }
    edges.sort( []( const Edge &a, const Edge &b ) { return a.weight > b.weight; } );

    list <Edge> :: iterator it; 
    Graph* graph = createGraph(dim, numOfEdges); 
    int i = 0;
    for(it = edges.begin(); it != edges.end(); ++it) {
        graph->edges[i].src = it->src;
        graph->edges[i].weight = 1;
        graph->edges[i++].dest = it->dest;
    }
    showlist(edges);
         
    KruskalMST(graph);

    delete(graph->edges);
    delete(graph);

    return 0;
}